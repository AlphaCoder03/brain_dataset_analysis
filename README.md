# brain_dataset_analysis

This loads brain.csv into pandas data frame, checks for null values in the dataset, extract the head-size & brain-weight into column vectors and plots a figure.