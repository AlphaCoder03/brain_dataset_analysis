"""
04/16/2020
@author: Venugopal Reddy Alamuru
"""

import matplotlib.pyplot as plt
import pandas as pd


print('\n*** DATASET LOADING STEP ***\n')
data = pd.read_csv('brain.csv')
print('printing first 5 rows of the dataset')
print("--------------------------------------")
print(data.head())
print("--------------------------------------")

print('\n*** DATA PROCESSING STEP ***\n')
print('Lets check if the total count of missing values by each column in the dataset:\n')
print(data.isnull().sum())

print('\n*** DATA EXTRACTION STEP ***\n')
headSize = data[['HeadSize']]
brainWeight = data[['BrainWeight']]
print('Head Size column vector shape: ', headSize.shape)
print(headSize)
print('Brain Weight column vector shape: ', brainWeight.shape)
print(brainWeight)

print('\nCalculating Min, Max & Mean of Head Size data...', headSize.agg(['min', 'max', 'mean']))
print('\nCalculating Min, Max & Mean of Brain Weight data...', brainWeight.agg(['min', 'max', 'mean']))

print('\n*** DATA VISUALIZATION STEP ***\n')
plt.scatter(headSize, brainWeight, marker='x', color='red')
plt.xlabel('Head Size')
plt.ylabel('Brain Weight')
plt.title('Relation between \'Head Size\' & \'Brain Weight\'')
plt.savefig('relation_between_head_size_and_brain_weight.png')
plt.show()
print('Observe the plotting between Head Size vs Brain Weight in graph created by the name:',
      'relation_between_head_size_and_brain_weight.png')
